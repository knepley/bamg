#
#  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#  BAMG - Bootstrap AMG in PETSc
#
#  This file is part of BAMG.
#  BAMG is distributed under the PETSc license.
#  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#

ALL: all
LOCDIR = ./
DIRS   = src include docs

# Include the rest of makefiles
include ./${PETSC_ARCH}/lib/bamg/conf/bamgvariables
include ${BAMG_DIR}/lib/bamg/conf/bamg_common

# This makefile doesn't really do any work. Sub-makes still benefit from parallelism.
.NOTPARALLEL:

#
# Basic targets to build bamg library
all:
	@${OMAKE} PETSC_ARCH=${PETSC_ARCH} PETSC_DIR=${PETSC_DIR} BAMG_DIR=${BAMG_DIR} chk_petscdir chk_bamgdir | tee ${PETSC_ARCH}/lib/bamg/conf/make.log
	@ln -sf ${PETSC_ARCH}/lib/bamg/conf/make.log make.log
	+@${OMAKE_PRINTDIR} PETSC_ARCH=${PETSC_ARCH} PETSC_DIR=${PETSC_DIR} BAMG_DIR=${BAMG_DIR} all-local 2>&1 | tee -a ${PETSC_ARCH}/lib/bamg/conf/make.log;
	@egrep -i "( error | error: |no such file or directory)" ${PETSC_ARCH}/lib/bamg/conf/make.log | tee ./${PETSC_ARCH}/lib/bamg/conf/error.log > /dev/null
	+@if test -s ./${PETSC_ARCH}/lib/bamg/conf/error.log; then \
           printf ${PETSC_TEXT_HILIGHT}"*******************************ERROR************************************\n" 2>&1 | tee -a ${PETSC_ARCH}/lib/bamg/conf/make.log; \
           echo "  Error during compile, check ${PETSC_ARCH}/lib/bamg/conf/make.log" 2>&1 | tee -a ${PETSC_ARCH}/lib/bamg/conf/make.log; \
           echo "  Send all contents of ./${PETSC_ARCH}/lib/bamg/conf to petsc-maint@mcs.anl.gov" 2>&1 | tee -a ${PETSC_ARCH}/lib/bamg/conf/make.log;\
           printf "************************************************************************"${PETSC_TEXT_NORMAL}"\n" 2>&1 | tee -a ${PETSC_ARCH}/lib/bamg/conf/make.log; \
	 elif [ "${BAMG_INSTALLDIR}" = "${BAMG_DIR}/${PETSC_ARCH}" ]; then \
           echo "Now to check if the library is working do:";\
           echo "make BAMG_DIR=${BAMG_DIR} PETSC_DIR=${PETSC_DIR} check";\
           echo "=========================================";\
	 else \
	   echo "Now to install the library do:";\
	   echo "make BAMG_DIR=${BAMG_DIR} PETSC_DIR=${PETSC_DIR} install";\
	   echo "=========================================";\
	 fi
	@echo "Finishing make run at `date +'%a, %d %b %Y %H:%M:%S %z'`" >> ${PETSC_ARCH}/lib/bamg/conf/make.log
	@if test -s ./${PETSC_ARCH}/lib/bamg/conf/error.log; then exit 1; fi

all-local: info bamg_libs

#
# Prints information about the system and version of BAMG being compiled
#
info:
	-@echo "=========================================="
	-@echo Starting make run on `hostname` at `date +'%a, %d %b %Y %H:%M:%S %z'`
	-@echo Machine characteristics: `uname -a`
	-@echo "-----------------------------------------"
	-@echo "Using BAMG directory: ${BAMG_DIR}"
	-@echo "Using PETSc directory: ${PETSC_DIR}"
	-@echo "Using PETSc arch: ${PETSC_ARCH}"
	-@echo "-----------------------------------------"
	-@grep "define BAMG_VERSION" ${BAMG_DIR}/include/bamgversion.h | ${SED} "s/........//"
	-@echo "-----------------------------------------"
	-@echo "Using BAMG configure options: ${BAMG_CONFIGURE_OPTIONS}"
	-@echo "Using BAMG configuration flags:"
	-@grep "\#define " ${BAMGCONF_H}
	-@echo "-----------------------------------------"
	-@grep "define PETSC_VERSION" ${PETSC_DIR}/include/petscversion.h | ${SED} "s/........//"
	-@echo "-----------------------------------------"
	-@echo "Using PETSc configure options: ${CONFIGURE_OPTIONS}"
	-@echo "Using PETSc configuration flags:"
	-@grep "\#define " ${PETSCCONF_H}
	-@echo "-----------------------------------------"
	-@echo "Using C/C++ include paths: ${BAMG_CC_INCLUDES}"
	-@echo "Using C/C++ compiler: ${PCC} ${PCC_FLAGS} ${COPTFLAGS} ${CFLAGS}"
	-@if [ "${FC}" != "" ]; then \
	   echo "Using Fortran include/module paths: ${BAMG_FC_INCLUDES}";\
	   echo "Using Fortran compiler: ${FC} ${FC_FLAGS} ${FFLAGS} ${FPP_FLAGS}";\
         fi
	-@echo "-----------------------------------------"
	-@echo "Using C/C++ linker: ${PCC_LINKER}"
	-@echo "Using C/C++ flags: ${PCC_LINKER_FLAGS}"
	-@if [ "${FC}" != "" ]; then \
	   echo "Using Fortran linker: ${FC_LINKER}";\
	   echo "Using Fortran flags: ${FC_LINKER_FLAGS}";\
         fi
	-@echo "-----------------------------------------"
	-@echo "Using libraries: ${BAMG_LIB}"
	-@echo "------------------------------------------"
	-@echo "Using mpiexec: ${MPIEXEC}"
	-@echo "------------------------------------------"
	-@echo "Using MAKE: ${MAKE}"
	-@echo "Default MAKEFLAGS: MAKE_NP:${MAKE_NP} MAKE_LOAD:${MAKE_LOAD} MAKEFLAGS:${MAKEFLAGS}"
	-@echo "=========================================="

# Simple test examples for checking a correct installation
check:
	-@${OMAKE} PATH="${PETSC_DIR}/${PETSC_ARCH}/lib:${BAMG_DIR}/${PETSC_ARCH}/lib:${PATH}" PETSC_ARCH=${PETSC_ARCH} PETSC_DIR=${PETSC_DIR} BAMG_DIR=${BAMG_DIR} check_build 2>&1 | tee ./${PETSC_ARCH}/lib/bamg/conf/check.log
check_build:
	-@echo "Running test examples to verify correct installation"
	-@echo "Using BAMG_DIR=${BAMG_DIR}, PETSC_DIR=${PETSC_DIR} and PETSC_ARCH=${PETSC_ARCH}"
	+@cd src/eps/tests && \
         ${OMAKE} PETSC_ARCH=${PETSC_ARCH} PETSC_DIR=${PETSC_DIR} BAMG_DIR=${BAMG_DIR} testtest10 && \
	 egrep "^#define PETSC_HAVE_FORTRAN 1" ${PETSCCONF_H} | tee .ftn.log > /dev/null; \
         if test -s .ftn.log; then \
           ${OMAKE} PETSC_ARCH=${PETSC_ARCH} PETSC_DIR=${PETSC_DIR} BAMG_DIR=${BAMG_DIR} testtest7f; \
         fi ; ${RM} .ftn.log && \
	 if [ "${BLOPEX_LIB}" != "" ]; then \
           ${OMAKE} PETSC_ARCH=${PETSC_ARCH} PETSC_DIR=${PETSC_DIR} BAMG_DIR=${BAMG_DIR} testtest5_blopex; \
         fi
	-@echo "Completed test examples"

# Compare ABI/API of two versions of PETSc library with the old one defined by PETSC_{DIR,ARCH}_ABI_OLD
abitest:
	@if [ "x${BAMG_DIR_ABI_OLD}" = "x" ] || [ "x${PETSC_ARCH_ABI_OLD}" = "x" ] || [ "x${PETSC_DIR_ABI_OLD}" = "x" ]; \
		then printf "You must set environment variables BAMG_DIR_ABI_OLD, PETSC_ARCH_ABI_OLD and PETSC_DIR_ABI_OLD to run abitest\n"; \
		exit 1; \
	fi;
	-@echo "Comparing ABI/API of the following two BAMG versions (you must have already configured and built them using GCC and with -g):"
	-@echo "========================================================================================="
	-@echo "    Old: BAMG_DIR_ABI_OLD  = ${BAMG_DIR_ABI_OLD}"
	-@echo "         PETSC_ARCH_ABI_OLD = ${PETSC_ARCH_ABI_OLD}"
	-@echo "         PETSC_DIR_ABI_OLD  = ${PETSC_DIR_ABI_OLD}"
	-@cd ${BAMG_DIR_ABI_OLD}; echo "         Branch             = "`git rev-parse --abbrev-ref HEAD`
	-@echo "    New: BAMG_DIR          = ${BAMG_DIR}"
	-@echo "         PETSC_ARCH         = ${PETSC_ARCH}"
	-@echo "         PETSC_DIR          = ${PETSC_DIR}"
	-@echo "         Branch             = "`git rev-parse --abbrev-ref HEAD`
	-@echo "========================================================================================="
	-@$(PYTHON)	${BAMG_DIR}/lib/bamg/bin/maint/abicheck.py -old_dir ${BAMG_DIR_ABI_OLD} -old_arch ${PETSC_ARCH_ABI_OLD} -old_petsc_dir ${PETSC_DIR_ABI_OLD} -new_dir ${BAMG_DIR} -new_arch ${PETSC_ARCH} -new_petsc_dir ${PETSC_DIR} -report_format html

# Deletes BAMG library
deletelibs:
	-${RM} -r ${BAMG_LIB_DIR}/libpetscbamg*.*
deletemods:
	-${RM} -f ${BAMG_DIR}/${PETSC_ARCH}/include/bamg*.mod

allclean:
	-@${OMAKE} -f gmakefile clean

clean:: allclean

reconfigure:
	@unset MAKEFLAGS && ${PYTHON} ${PETSC_ARCH}/lib/bamg/conf/reconfigure-${PETSC_ARCH}.py

#
# Check if PETSC_DIR variable specified is valid
#
chk_petsc_dir:
	@if [ ! -f ${PETSC_DIR}/include/petscversion.h ]; then \
          printf ${PETSC_TEXT_HILIGHT}"*************************ERROR**************************************\n"; \
	  echo "Incorrect PETSC_DIR specified: ${PETSC_DIR}!                             "; \
	  echo "You need to use / to separate directories, not \\!                       "; \
	  echo "Aborting build                                                           "; \
          printf "********************************************************************"${PETSC_TEXT_NORMAL}"\n"; \
	  false; fi
#
# Check if BAMG_DIR variable specified is valid
#
chk_bamg_dir:
	@if [ ! -f ${BAMG_DIR}/include/bamgversion.h ]; then \
          printf ${PETSC_TEXT_HILIGHT}"*************************ERROR**************************************\n"; \
	  echo "Incorrect BAMG_DIR specified: ${BAMG_DIR}!                             "; \
	  echo "You need to use / to separate directories, not \\!                       "; \
	  echo "Aborting build                                                           "; \
          printf "********************************************************************"${PETSC_TEXT_NORMAL}"\n"; \
	  false; fi
#
# Install relevant files in the prefix directory
#
install:
	@${PYTHON} ./config/install.py ${BAMG_DIR} ${PETSC_DIR} ${BAMG_INSTALLDIR} -destDir=${DESTDIR} ${PETSC_ARCH} ${AR_LIB_SUFFIX} ${RANLIB};

# ------------------------------------------------------------------
#
# All remaining actions are intended for BAMG developers only.
# BAMG users should not generally need to use these commands.
#

# Builds all the documentation
alldoc: alldoc1 alldoc2

# Build everything that goes into 'doc' dir except html sources
alldoc1: chk_loc deletemanualpages
	-${OMAKE} ACTION=manualpages_buildcite tree_basic LOC=${LOC}
	-@sed -e s%man+../%man+manualpages/% ${LOC}/docs/manualpages/manualpages.cit > ${LOC}/docs/manualpages/htmlmap
	-@cat ${PETSC_DIR}/src/docs/mpi.www.index >> ${LOC}/docs/manualpages/htmlmap
	-${OMAKE} ACTION=bamg_manualpages tree_basic LOC=${LOC}
	-${PYTHON} ${PETSC_DIR}/lib/petsc/bin/maint/wwwindex.py ${BAMG_DIR} ${LOC}
	-${OMAKE} ACTION=bamg_manexamples tree_basic LOC=${LOC}

# Builds .html versions of the source
alldoc2: chk_loc
	-${OMAKE} ACTION=bamg_html PETSC_DIR=${PETSC_DIR} alltree LOC=${LOC}
	cp ${LOC}/docs/manual.html ${LOC}/docs/index.html

# modify all generated html files and add in version number, date, canonical URL info.
docsetdate: chk_petscdir
	@echo "Updating generated html files with bamg version, date, canonical URL info";\
        version_release=`grep '^#define BAMG_VERSION_RELEASE ' include/bamgversion.h |tr -s ' ' | cut -d ' ' -f 3`; \
        version_major=`grep '^#define BAMG_VERSION_MAJOR ' include/bamgversion.h |tr -s ' ' | cut -d ' ' -f 3`; \
        version_minor=`grep '^#define BAMG_VERSION_MINOR ' include/bamgversion.h |tr -s ' ' | cut -d ' ' -f 3`; \
        version_subminor=`grep '^#define BAMG_VERSION_SUBMINOR ' include/bamgversion.h |tr -s ' ' | cut -d ' ' -f 3`; \
        if  [ $${version_release} = 0 ]; then \
          bamgversion=bamg-dev; \
          export bamgversion; \
        elif [ $${version_release} = 1 ]; then \
          bamgversion=bamg-$${version_major}.$${version_minor}.$${version_subminor}; \
          export bamgversion; \
        else \
          echo "Unknown BAMG_VERSION_RELEASE: $${version_release}"; \
          exit; \
        fi; \
        datestr=`git log -1 --pretty=format:%ci | cut -d ' ' -f 1`; \
        export datestr; \
        gitver=`git describe`; \
        export gitver; \
        find * -type d -wholename 'arch-*' -prune -o -type f -name \*.html \
          -exec perl -pi -e 's^(<body.*>)^$$1\n   <div id=\"version\" align=right><b>$$ENV{bamgversion} $$ENV{datestr}</b></div>\n   <div id="bugreport" align=right><a href="mailto:petsc-maint\@mcs.anl.gov?subject=Typo or Error in Documentation &body=Please describe the typo or error in the documentation: $$ENV{bamgversion} $$ENV{gitver} {} "><small>Report Typos and Errors</small></a></div>^i' {} \; \
          -exec perl -pi -e 's^(<head>)^$$1 <link rel="canonical" href="https://mcs.anl.gov/petsc/documentation/current/{}" />^i' {} \; ; \
        echo "Done fixing version number, date, canonical URL info"

# Deletes documentation
alldocclean: deletemanualpages allcleanhtml
deletemanualpages: chk_loc
	-@if [ -d ${LOC} -a -d ${LOC}/docs/manualpages ]; then \
          find ${LOC}/docs/manualpages -type f -name "*.html" -exec ${RM} {} \; ;\
          ${RM} ${LOC}/docs/manualpages/manualpages.cit ;\
        fi
allcleanhtml:
	-${OMAKE} ACTION=cleanhtml PETSC_DIR=${PETSC_DIR} alltree

# Builds Fortran stub files
allfortranstubs:
	-@${RM} -rf ${PETSC_ARCH}/include/bamg/finclude/ftn-auto/*-tmpdir
	@${PYTHON} ${BAMG_DIR}/lib/bamg/bin/maint/generatefortranstubs.py ${BFORT} ${VERBOSE}
	-@${PYTHON} ${BAMG_DIR}/lib/bamg/bin/maint/generatefortranstubs.py -merge ${VERBOSE}
	-@${RM} -rf include/bamg/finclude/ftn-auto/*-tmpdir
deletefortranstubs:
	-@find . -type d -name ftn-auto | xargs rm -rf

check_output:
	-@${OMAKE} PETSC_ARCH=${PETSC_ARCH} PETSC_DIR=${PETSC_DIR} BAMG_DIR=${BAMG_DIR} -f gmakefile.test check_output

check_ascii:
	@ ! git --no-pager grep -l -I -P "[^\x00-\x7F]"

checkbadSource_bamg:
	-@${OMAKE} PETSC_ARCH=${PETSC_ARCH} PETSC_DIR=${PETSC_DIR} BAMG_DIR=${BAMG_DIR} checkbadSource 2>&1
	@ exit `grep -c 'files with errors' checkbadSource.out`

# -------------------------------------------------------------------------------
#
# Some macros to check if the Fortran interface is up-to-date.
#
countfortranfunctions:
	-@for D in `find ${BAMG_DIR}/src -name ftn-auto` \
	`find ${BAMG_DIR}/src -name ftn-custom`; do cd $$D; \
	egrep '^void' *.c | \
	cut -d'(' -f1 | tr -s  ' ' | cut -d' ' -f3 | uniq | egrep -v "(^$$|Petsc)" | \
	sed "s/_$$//"; done | sort > /tmp/countfortranfunctions

countcfunctions:
	-@ ls ${BAMG_DIR}/include/*.h | grep -v bamgblaslapack.h | \
	xargs grep extern | grep "(" | tr -s ' ' | \
	cut -d'(' -f1 | cut -d' ' -f3 | grep -v "\*" | tr -s '\012' |  \
	tr 'A-Z' 'a-z' |  sort > /tmp/countcfunctions

difffortranfunctions: countfortranfunctions countcfunctions
	-@echo -------------- Functions missing in the Fortran interface ---------------------
	-@${DIFF} /tmp/countcfunctions /tmp/countfortranfunctions | grep "^<" | cut -d' ' -f2
	-@echo ----------------- Functions missing in the C interface ------------------------
	-@${DIFF} /tmp/countcfunctions /tmp/countfortranfunctions | grep "^>" | cut -d' ' -f2
	-@${RM}  /tmp/countcfunctions /tmp/countfortranfunctions

checkbadfortranstubs:
	-@echo "========================================="
	-@echo "Functions with MPI_Comm as an Argument"
	-@echo "========================================="
	-@for D in `find ${BAMG_DIR}/src -name ftn-auto`; do cd $$D; \
	grep '^void' *.c | grep 'MPI_Comm' | \
	tr -s ' ' | tr -s ':' ' ' |cut -d'(' -f1 | cut -d' ' -f1,3; done
	-@echo "========================================="
	-@echo "Functions with a String as an Argument"
	-@echo "========================================="
	-@for D in `find ${BAMG_DIR}/src -name ftn-auto`; do cd $$D; \
	grep '^void' *.c | grep 'char \*' | \
	tr -s ' ' | tr -s ':' ' ' |cut -d'(' -f1 | cut -d' ' -f1,3; done
	-@echo "========================================="
	-@echo "Functions with Pointers to PETSc Objects as Argument"
	-@echo "========================================="
	-@_p_OBJ=`grep _p_ ${PETSC_DIR}/include/*.h | tr -s ' ' | \
	cut -d' ' -f 3 | tr -s '\012' | grep -v '{' | cut -d'*' -f1 | \
	sed "s/_p_//g" | tr -s '\012 ' ' *|' ` ; \
	_p_OBJS=`grep _p_ ${BAMG_DIR}/include/*.h | tr -s ' ' | \
	cut -d' ' -f 3 | tr -s '\012' | grep -v '{' | cut -d'*' -f1 | \
	sed "s/_p_//g" | tr -s '\012 ' ' *|' ` ; \
	for D in `find ${BAMG_DIR}/src -name ftn-auto`; do cd $$D; \
	for OBJ in $$_p_OBJ $$_p_OBJS; do \
	grep "$$OBJ \*" *.c | tr -s ' ' | tr -s ':' ' ' | \
	cut -d'(' -f1 | cut -d' ' -f1,4; \
	done; done

# Generate tags
alletags:
	-@${PYTHON} ${BAMG_DIR}/lib/bamg/bin/maint/generateetags.py
	-@find config -type f -name "*.py" |grep -v SCCS | xargs etags -o TAGS_PYTHON

.PHONY: info all deletelibs allclean alletags alldoc allcleanhtml countfortranfunctions install
