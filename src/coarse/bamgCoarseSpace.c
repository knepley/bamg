#include <bamg.h>
#include <petscdm.h>
#include <petsc/private/pcmgimpl.h> /* For mg->eigenvalue */

PETSC_EXTERN PetscErrorCode PetscDLLibraryRegister_petscbamg(void)
{
  PetscFunctionBegin;
  PetscCall(PCMGRegisterCoarseSpaceConstructor("BAMG_EV",   BAMGCreateCoarseSpaceEV));
  PetscCall(PCMGRegisterCoarseSpaceConstructor("BAMG_MEV",  BAMGCreateCoarseSpaceMEV));
  PetscCall(PCMGRegisterCoarseSpaceConstructor("BAMG_GEV",  BAMGCreateCoarseSpaceGEV));
  PetscCall(PCMGRegisterCoarseSpaceConstructor("BAMG_MGEV", BAMGCreateCoarseSpaceMGEV));
  PetscFunctionReturn(0);
}


typedef struct {
  Mat         A;       /* The system operator */
  KSP         M;       /* The smoother */
  Vec         w, a, b; /* Work vectors */
  PetscScalar lambda;  /* The eigenvalue estimate */
  PetscBool   useM;    /* Calculate the action of \tilde M^{-1} A, instead of A */
} OpCtx;

/* This is intended to be the action of $\tilde M^{-1} A$ where
\begin{align*}
  \tilde M^{-1} = M^{-1} + M^{-T} - M^{-T} A M^{-1}
\end{align*}
is the symmetrized smoother for $M$, making
\begin{align*}
  \tilde M = M \left(M + M^T - A\right)^{-1} M^T.
\end{align*}
*/
static PetscErrorCode Minv_A_Private(Mat A, Vec x, Vec y)
{
  OpCtx *ctx;

  PetscFunctionBegin;
  PetscCall(MatShellGetContext(A, &ctx));
  if (ctx->useM) {
#if 0
    PetscCall(MatMult(ctx->A, x, ctx->w));
    PetscCall(KSPSolve(ctx->M, ctx->w, ctx->a));
    PetscCall(MatMult(ctx->A, ctx->a, ctx->b));
    PetscCall(VecAXPY(ctx->w, -1.0, ctx->b));
    PetscCall(KSPSolveTranspose(ctx->M, ctx->w, ctx->b));
    PetscCall(VecWAXPY(y, 1.0, ctx->b, ctx->a));
#else
    PetscCall(MatMult(ctx->A, x, ctx->w));
    PetscCall(KSPSolve(ctx->M, ctx->w, y));
#endif
  } else {
    PetscCall(MatMult(ctx->A, x, y));
  }
  PetscFunctionReturn(0);
}
static PetscErrorCode Minv_A_diagonal_Private(Mat A, Vec d)
{
  OpCtx *ctx;

  PetscFunctionBegin;
  PetscCall(MatShellGetContext(A, &ctx));
  PetscCheck(!ctx->useM, PetscObjectComm((PetscObject) A), PETSC_ERR_SUP, "Don't know how to get diagonal of \tilde M^{-1} A");
  PetscCall(MatGetDiagonal(ctx->A, d));
  PetscFunctionReturn(0);
}

/* This is intended to be the action of $\tilde M^{-1} A - \lambda I$ where */
static PetscErrorCode Minv_A_lambda_Private(Mat A, Vec x, Vec y)
{
  OpCtx *ctx;

  PetscFunctionBegin;
  PetscCall(MatShellGetContext(A, &ctx));
  PetscCall(Minv_A_Private(A, x, y));
  PetscCall(VecAXPY(y, -ctx->lambda, x));
  PetscFunctionReturn(0);
}
static PetscErrorCode Minv_A_lambda_diagonal_Private(Mat A, Vec d)
{
  OpCtx *ctx;

  PetscFunctionBegin;
  PetscCall(MatShellGetContext(A, &ctx));
  PetscCall(Minv_A_diagonal_Private(A, d));
  PetscCall(VecShift(d, -ctx->lambda));
  PetscFunctionReturn(0);
}

static PetscErrorCode PCMGCheckEigenvectors_Private(PC pc, PetscInt level, const char desc[], Mat S, PetscInt Nc, Mat coarseSpace)
{
  PC_MG *mg = (PC_MG *) pc->data;
  OpCtx *ctx;

  PetscFunctionBegin;
  PetscCall(MatShellGetContext(S, &ctx));
  /* Make \tilde M^{-1} A_l - \lambda^k_l I operator */
  PetscCall(MatShellSetOperation(S, MATOP_MULT, (void(*)(void)) Minv_A_lambda_Private));

  for (PetscInt i = 0; i < Nc; ++i) {
    Vec       cv;
    PetscReal residual;
    PetscBool flg;

    PetscCall(MatDenseGetColumnVecRead(coarseSpace, i, &cv));
    PetscCall(PetscObjectComposedDataGetScalar((PetscObject) cv, mg->eigenvalue, ctx->lambda, flg));
    PetscCheck(flg, PetscObjectComm((PetscObject) coarseSpace), PETSC_ERR_ARG_WRONG, "Eigenvalue not available in vector");
    PetscCall(MatMult(S, cv, ctx->w));
    PetscCall(VecNorm(ctx->w, NORM_2, &residual));
    PetscCall(PetscPrintf(PETSC_COMM_SELF, "%sL: %D Coarse Vec %D lambda %g EigenResidual %g\n", desc, level, i, ctx->lambda, residual));
  }
  PetscFunctionReturn(0);
}

/* A full Eigensolver for the coarse level of MG */
static PetscErrorCode BAMGCreateCoarseSpaceESP_Private(PC pc, PetscInt level, DM dm, PetscBool useM, KSP ksp, PetscInt Nc, Mat initialGuess, Mat *coarseSpace)
{
  PC_MG   *mg = (PC_MG *)pc->data;
  EPS      eps;
  Mat      S;
  OpCtx    ctx;
  PetscInt argc = 0, m, n, M, N, ncv;
  char   **argv = NULL;
  MPI_Comm comm;

  PetscFunctionBegin;
  PetscCall(SlepcInitialize(&argc, &argv, NULL, NULL));
  PetscCall(KSPGetOperators(ksp, &ctx.A, NULL));
  PetscCall(MatCreateVecs(ctx.A, NULL, &ctx.w));
  PetscCall(MatCreateVecs(ctx.A, NULL, &ctx.a));
  PetscCall(MatCreateVecs(ctx.A, NULL, &ctx.b));
  ctx.useM = useM;
  ctx.M    = ksp;

  PetscCall(PetscObjectGetComm((PetscObject) ctx.A, &comm));
  PetscCall(MatCreate(comm, &S));
  PetscCall(MatGetLocalSize(ctx.A, &m, &n));
  PetscCall(MatGetSize(ctx.A, &M, &N));
  PetscCall(MatSetSizes(S, m, n, M, N));
  PetscCall(MatSetType(S, MATSHELL));
  PetscCall(MatShellSetOperation(S, MATOP_MULT, (void(*)(void)) Minv_A_Private));
  PetscCall(MatShellSetContext(S, &ctx));
  PetscCall(MatSetUp(S));
  /* Create eigensolver */
  PetscCall(EPSCreate(comm, &eps));
  PetscCall(EPSSetOperators(eps, S, NULL));
  PetscCall(EPSSetProblemType(eps, EPS_HEP));
  PetscCall(EPSSetWhichEigenpairs(eps, EPS_SMALLEST_REAL));
  PetscCall(EPSSetDimensions(eps, Nc, PETSC_DEFAULT, PETSC_DEFAULT));
  PetscCall(EPSSetOptionsPrefix(eps, "pc_mg_adapt_interp_"));
  PetscCall(EPSSetFromOptions(eps));
  PetscCall(EPSSolve(eps));
  PetscCall(EPSGetConverged(eps, &ncv));
  PetscCheck(ncv >= Nc, comm, PETSC_ERR_LIB, "Number of converged eigenvectors %D < requested vectors %D", ncv, Nc);
  if (!*coarseSpace) PetscCall(MatCreateDense(comm, m, PETSC_DETERMINE, M, ncv, NULL, coarseSpace));
  for (PetscInt i = 0; i < PetscMin(ncv, Nc); ++i) {
    Vec         cv;
    PetscScalar lambda;
    PetscReal   kr;

    PetscCall(MatDenseGetColumnVecWrite(*coarseSpace, i, &cv));
    PetscCall(EPSGetEigenpair(eps, i, &kr, NULL, cv, NULL));
    lambda = kr;
    // TODO This composition no longer works
    PetscCall(PetscObjectComposedDataSetScalar((PetscObject) cv, mg->eigenvalue, lambda));
    PetscCall(MatDenseRestoreColumnVecWrite(*coarseSpace, i, &cv));
  }
  if (mg->mespMonitor) {PetscCall(PCMGCheckEigenvectors_Private(pc, level, "Direct solve ", S, Nc, *coarseSpace));}
  /* Cleanup */
  PetscCall(VecDestroy(&ctx.w));
  PetscCall(VecDestroy(&ctx.a));
  PetscCall(VecDestroy(&ctx.b));
  PetscCall(MatDestroy(&S));
  PetscCall(EPSDestroy(&eps));
  PetscFunctionReturn(0);
}

/* The Multilevel Eigensolver for intermediate MG levels */
static PetscErrorCode BAMGCreateCoarseSpaceMESP_Private(PC pc, PetscInt level, DM dm, PetscBool useM, KSP ksp, PetscInt Nc, Mat initialGuess, Mat *coarseSpace)
{
  PC_MG   *mg = (PC_MG *) pc->data;
  BV       bv;
  KSP      esmooth;
  Mat      Interp, S;
  Vec      tmp;
  OpCtx    ctx;
  PetscInt m, n, M, N;
  MPI_Comm comm;

  PetscFunctionBegin;
  PetscCall(KSPGetOperators(ksp, &ctx.A, NULL));
  PetscCall(PetscObjectGetComm((PetscObject) ctx.A, &comm));
  PetscCall(MatCreateVecs(ctx.A, NULL, &ctx.w));
  PetscCall(MatCreateVecs(ctx.A, NULL, &ctx.a));
  PetscCall(MatCreateVecs(ctx.A, NULL, &ctx.b));
  ctx.useM = useM;
  ctx.M    = ksp;

  PetscCall(MatSetOption(ctx.A, MAT_SYMMETRIC, PETSC_TRUE));

  /* Make \tilde M^{-1} A_l - \lambda^k_l I operator */
  PetscCall(MatCreate(comm, &S));
  PetscCall(MatGetLocalSize(ctx.A, &m, &n));
  PetscCall(MatGetSize(ctx.A, &M, &N));
  PetscCall(MatSetSizes(S, m, n, M, N));
  PetscCall(MatSetType(S, MATSHELL));
  PetscCall(MatShellSetOperation(S, MATOP_MULT, (void(*)(void)) Minv_A_lambda_Private));
  PetscCall(MatShellSetOperation(S, MATOP_GET_DIAGONAL, (void(*)(void)) Minv_A_lambda_diagonal_Private));
  PetscCall(MatShellSetContext(S, &ctx));
  PetscCall(MatSetUp(S));

  PetscCall(KSPCreate(comm, &esmooth));
  PetscCall(KSPSetOptionsPrefix(esmooth, "pc_mg_mesp_"));
  PetscCall(KSPSetInitialGuessNonzero(esmooth, PETSC_TRUE));
  PetscCall(KSPSetFromOptions(esmooth));
  PetscCall(KSPSetOperators(esmooth, S, S));

  PetscCall(PCMGGetInterpolation(pc, level, &Interp));
  if (!*coarseSpace) PetscCall(MatCreateDense(comm, m, PETSC_DETERMINE, M, Nc, NULL, coarseSpace));
  PetscCall(DMGetGlobalVector(dm, &tmp));
  for (PetscInt i = 0; i < Nc; ++i) {
    Vec         cv, iv;
    PetscScalar lambda;
    PetscReal   norm;
    PetscBool   flg;

    PetscCall(MatDenseGetColumnVecRead(initialGuess, i, &iv));
    PetscCall(MatDenseGetColumnVecWrite(*coarseSpace, i, &cv));
    PetscCall(MatInterpolate(Interp, iv, cv));
    PetscCall(VecNormalize(cv, &norm));
    // TODO This composition no longer works
    PetscCall(PetscObjectComposedDataGetScalar((PetscObject) iv,   mg->eigenvalue, lambda, flg));
    PetscCheck(flg, comm, PETSC_ERR_ARG_WRONG, "Eigenvalue not available in vector");
    PetscCall(PetscObjectComposedDataSetScalar((PetscObject) cv, mg->eigenvalue, lambda));
    PetscCall(MatDenseRestoreColumnVecRead(initialGuess, i, &iv));
    PetscCall(MatDenseRestoreColumnVecWrite(*coarseSpace, i, &cv));
  }
  if (mg->mespMonitor) {PetscCall(PCMGCheckEigenvectors_Private(pc, level, "After interpolation ", S, Nc, *coarseSpace));}
  PetscCall(BVCreate(comm, &bv));
  {
    Vec cv;
    PetscCall(MatDenseGetColumnVecRead(*coarseSpace, 0, &cv));
    PetscCall(BVSetSizesFromVec(bv, cv, Nc));
    PetscCall(MatDenseRestoreColumnVecRead(*coarseSpace, 0, &cv));
  }
  PetscCall(BVSetFromOptions(bv));
  for (PetscInt i = 0; i < Nc; ++i) {
    Vec         ev;
    PetscScalar lambda;
    PetscInt    K = 5;
    PetscBool   flg;

    PetscCall(MatDenseGetColumnVecRead(*coarseSpace, i, &ev));
    PetscCall(PetscObjectComposedDataGetScalar((PetscObject) ev, mg->eigenvalue, lambda, flg));
    PetscCheck(flg, comm, PETSC_ERR_ARG_WRONG, "Eigenvalue not available in vector");
    PetscCall(BVInsertVec(bv, i, ev));
    PetscCall(BVOrthonormalizeColumn(bv, i, PETSC_FALSE, NULL, NULL));
    PetscCall(BVCopyVec(bv, i, ev));
    /* This loop should become a new EPS solver */
    for (PetscInt k = 0; k < K; ++k) {
      PetscReal numer, denom;

      ctx.lambda = lambda;
      /* Smooth (A_l - \lambda^k_l \tilde M_l) w^k_l = 0 to update eigenvector w^k_l */
      PetscCall(VecSet(tmp, 0.0));
      PetscCall(KSPSolve(esmooth, tmp, ev));
      /* Correct eigenvalue \lambda^k_l = <A_l w^k_l, w^k_l>_2 / <T_l w^k_l, w^k_l>_2 */
      PetscCall(Minv_A_Private(S, ev, tmp));
      PetscCall(VecDot(ev, tmp, &numer));
      PetscCall(VecDot(ev, ev,  &denom));
      lambda = numer / denom;
      if (mg->mespMonitor) {PetscCall(PetscPrintf(PETSC_COMM_SELF, "L: %D Coarse vec %D mesp it: %D Old lambda: %g New lambda: %g\n", level, i, k, ctx.lambda, lambda));}
      /* Normalize guess */
      PetscCall(VecScale(ev, 1.0/PetscSqrtReal(denom)));
    }
    PetscCall(PetscObjectComposedDataSetScalar((PetscObject) ev, mg->eigenvalue, lambda));
    PetscCall(BVInsertVec(bv, i, ev));
    PetscCall(BVOrthonormalizeColumn(bv, i, PETSC_FALSE, NULL, NULL));
    PetscCall(MatDenseRestoreColumnVecRead(*coarseSpace, i, &ev));
  }
  if (mg->mespMonitor) {PetscCall(PCMGCheckEigenvectors_Private(pc, level, "After smoothing ", S, Nc, *coarseSpace));}
  PetscCall(DMRestoreGlobalVector(dm, &tmp));
  /* Cleanup */
  PetscCall(BVDestroy(&bv));
  PetscCall(KSPDestroy(&esmooth));
  PetscCall(VecDestroy(&ctx.w));
  PetscCall(VecDestroy(&ctx.a));
  PetscCall(VecDestroy(&ctx.b));
  PetscCall(MatDestroy(&S));
  PetscFunctionReturn(0);
}

PetscErrorCode BAMGCreateCoarseSpaceEV(PC pc, PetscInt level, DM dm, KSP ksp, PetscInt Nc, Mat initialGuess, Mat *coarseSpace)
{
  PetscFunctionBegin;
  PetscCall(BAMGCreateCoarseSpaceESP_Private(pc, level, dm, PETSC_FALSE, ksp, Nc, initialGuess, coarseSpace));
  PetscFunctionReturn(0);
}

PetscErrorCode BAMGCreateCoarseSpaceGEV(PC pc, PetscInt level, DM dm, KSP ksp, PetscInt Nc, Mat initialGuess, Mat *coarseSpace)
{
  PetscFunctionBegin;
  PetscCall(BAMGCreateCoarseSpaceESP_Private(pc, level, dm, PETSC_TRUE, ksp, Nc, initialGuess, coarseSpace));
  PetscFunctionReturn(0);
}

PetscErrorCode BAMGCreateCoarseSpaceMEV(PC pc, PetscInt level, DM dm, KSP ksp, PetscInt Nc, Mat initialGuess, Mat *coarseSpace)
{
  PetscFunctionBegin;
  PetscCall(BAMGCreateCoarseSpaceMESP_Private(pc, level, dm, PETSC_FALSE, ksp, Nc, initialGuess, coarseSpace));
  PetscFunctionReturn(0);
}

PetscErrorCode BAMGCreateCoarseSpaceMGEV(PC pc, PetscInt level, DM dm, KSP ksp, PetscInt Nc, Mat initialGuess, Mat *coarseSpace)
{
  PetscFunctionBegin;
  PetscCall(BAMGCreateCoarseSpaceMESP_Private(pc, level, dm, PETSC_TRUE, ksp, Nc, initialGuess, coarseSpace));
  PetscFunctionReturn(0);
}
