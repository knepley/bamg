#!/usr/bin/env python
#
#  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#  BAMG - Bootstrap AMG in PETSc
#
#  This file is part of BAMG.
#  BAMG is distributed under the PETSc license.
#  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
#

from __future__ import print_function
import os, sys, time, shutil

def AddDefine(conffile,name,value,prefix='BAMG_'):
  conffile.write('#define '+prefix+name+' "'+value+'"\n')

def WriteModulesFile(modules,version,sdir):
  ''' Write the contents of the Modules file '''
  modules.write('#%Module\n\n')
  modules.write('proc ModulesHelp { } {\n')
  modules.write('    puts stderr "This module sets the path and environment variables for bamg-%s"\n' % version)
  modules.write('    puts stderr "     see https://gitlab.com/petsc/bamg for more information"\n')
  modules.write('    puts stderr ""\n}\n')
  modules.write('module-whatis "BAMG - Bootstrap AMG for PETSc"\n\n')
  modules.write('module load petsc\n')
  modules.write('set bamg_dir %s\n' % sdir)
  modules.write('setenv BAMG_DIR $bamg_dir\n')

def WritePkgconfigFile(pkgconfig,version,pversion,sdir,isinstall,prefixdir):
  ''' Write the contents of the pkg-config file '''
  pkgconfig.write('prefix=%s\n' % prefixdir)
  pkgconfig.write('exec_prefix=${prefix}\n')
  pkgconfig.write('includedir=${prefix}/include\n')
  pkgconfig.write('libdir=${prefix}/lib\n\n')
  pkgconfig.write('Name: BAMG\n')
  pkgconfig.write('Description: Bootstrap AMG for PETSc\n')
  pkgconfig.write('Version: %s\n' % version)
  pkgconfig.write('Requires: PETSc >= %s\n' % pversion)
  pkgconfig.write('Cflags: -I${includedir}')
  if not isinstall:
    pkgconfig.write(' -I'+os.path.join(sdir,'include'))
  pkgconfig.write('\nLibs:')
  pkgconfig.write(' -L${libdir} -lpetscbamg\n')

def WriteReconfigScript(reconfig,bamgdir,usedargs):
  ''' Write the contents of the reconfigure script '''
  reconfig.write('#!/usr/bin/env python\n\n')
  reconfig.write('import os, sys\n')
  if usedargs:
    reconfig.write('sys.argv.extend(\''+usedargs+'\'.split())\n')
  reconfig.write('execfile(os.path.join(\''+bamgdir+'\',\'config\',\'configure.py\'))\n')

# Use en_US as language so that compiler messages are in English
if 'LC_LOCAL' in os.environ and os.environ['LC_LOCAL'] != '' and os.environ['LC_LOCAL'] != 'en_US' and os.environ['LC_LOCAL']!= 'en_US.UTF-8': os.environ['LC_LOCAL'] = 'en_US.UTF-8'
if 'LANG' in os.environ and os.environ['LANG'] != '' and os.environ['LANG'] != 'en_US' and os.environ['LANG'] != 'en_US.UTF-8': os.environ['LANG'] = 'en_US.UTF-8'

# Check python version
if sys.version_info < (2,6) or (sys.version_info >= (3,0) and sys.version_info < (3,4)):
  print('*******************************************************************************')
  print('*        Python version 2.6+ or 3.4+ is required to run ./configure           *')
  print('*           Try: "python2.7 ./configure" or "python3 ./configure"             *')
  print('*******************************************************************************')
  sys.exit(4)

# Set python path
configdir = os.path.abspath('config')
if not os.path.isdir(configdir):
  sys.exit('ERROR: Run configure from $BAMG_DIR, not '+os.path.abspath('.'))
sys.path.insert(0,configdir)
sys.path.insert(0,os.path.join(configdir,'packages'))

# Load auxiliary classes
import argdb, log
argdb = argdb.ArgDB(sys.argv)
log   = log.Log()

# Load classes for packages and process command-line options
import bamg, petsc, lapack, sowing
bamg   = bamg.BAMG(argdb,log)
petsc  = petsc.PETSc(argdb,log)
lapack = lapack.Lapack(argdb,log)
sowing = sowing.Sowing(argdb,log)

externalpackages = []
optionspackages  = [bamg, sowing] + externalpackages
checkpackages    = externalpackages + [lapack]

# Print help if requested and check for wrong command-line options
if argdb.PopHelp():
  print('BAMG Configure Help')
  print('-'*80)
  for pkg in optionspackages:
    pkg.ShowHelp()
  sys.exit(0)
argdb.ErrorPetscOptions()
argdb.ErrorIfNotEmpty()

# Check enviroment and PETSc version
log.Print('Checking environment...')
petsc.InitDir(bamg.prefixdir)
bamg.InitDir()
petsc.LoadVersion()
bamg.LoadVersion()

# Load PETSc configuration
petsc.LoadConf()

# Check for empty PETSC_ARCH
emptyarch = not ('PETSC_ARCH' in os.environ and os.environ['PETSC_ARCH'])
if emptyarch:
  pseudoarch = 'arch-' + sys.platform.replace('cygwin','mswin')+ '-' + petsc.language.lower().replace('+','x')
  if petsc.debug:
    pseudoarch += '-debug'
  else:
    pseudoarch += '-opt'
  if not 'real' in petsc.scalar:
    pseudoarch += '-' + petsc.scalar
  archname = 'installed-'+pseudoarch
else:
  archname = petsc.arch

# Create directories for configuration files
archdir, archdirexisted = bamg.CreateDirTest(bamg.dir,archname)
libdir  = bamg.CreateDir(archdir,'lib')
confdir = bamg.CreateDirTwo(libdir,'bamg','conf')

# Open log file
log.Open(bamg.dir,confdir,'configure.log')
log.write('='*80)
log.write('Starting Configure Run at '+time.ctime(time.time()))
log.write('Configure Options: '+' '.join(sys.argv[1:]))
log.write('Working directory: '+os.getcwd())
log.write('Python version:\n'+sys.version)
log.write('make: '+petsc.make)

# Some checks related to PETSc configuration
if petsc.nversion < bamg.nversion:
  log.Exit('This BAMG version is not compatible with PETSc version '+petsc.version)
if not petsc.precision in ['double','single','__float128']:
  log.Exit('This BAMG version does not work with '+petsc.precision+' precision')

# Display versions and paths
log.write('PETSc source directory: '+petsc.dir)
log.write('PETSc install directory: '+petsc.prefixdir)
log.write('PETSc version: '+petsc.lversion)
if not emptyarch:
  log.write('PETSc architecture: '+petsc.arch)
log.write('BAMG source directory: '+bamg.dir)
if bamg.isinstall:
  log.write('BAMG install directory: '+bamg.prefixdir)
log.write('BAMG version: '+bamg.lversion)

# Clean previous configuration if needed
if archdirexisted:
  if bamg.isinstall and not bamg.clean:
    log.Exit('You are requesting a prefix install but the arch directory '+archdir+' already exists and may contain files from previous builds; consider adding option --with-clean')
  if bamg.clean:
    log.Println('\nCleaning arch dir '+archdir+'...')
    try:
      for root, dirs, files in os.walk(archdir,topdown=False):
        for name in files:
          if name!='configure.log':
            os.remove(os.path.join(root,name))
    except:
      log.Exit('Cannot remove existing files in '+archdir)
    for rdir in ['obj','externalpackages']:
      try:
        shutil.rmtree(os.path.join(archdir,rdir))
      except: pass

# Create other directories and configuration files
if not bamg.prefixdir:
  bamg.prefixdir = archdir
includedir = bamg.CreateDir(archdir,'include')
modulesdir = bamg.CreateDirTwo(confdir,'modules','bamg')
pkgconfdir = bamg.CreateDir(libdir,'pkgconfig')
bamgvars  = bamg.CreateFile(confdir,'bamgvariables')
bamgconf  = bamg.CreateFile(includedir,'bamgconf.h')
pkgconfig  = bamg.CreateFile(pkgconfdir,'bamg.pc')
if bamg.isinstall:
  modules  = bamg.CreateFile(modulesdir,bamg.lversion)
else:
  modules  = bamg.CreateFile(modulesdir,bamg.lversion+'-'+archname)
  reconfig = bamg.CreateFile(confdir,'reconfigure-'+archname+'.py')
  reconfigpath = os.path.join(confdir,'reconfigure-'+archname+'.py')

# Write initial part of file bamgvariables
bamgvars.write('BAMG_CONFIGURE_OPTIONS = '+argdb.UsedArgs()+'\n')
bamgvars.write('BAMG_INSTALLDIR = '+bamg.prefixdir+'\n')
if emptyarch:
  bamgvars.write('INSTALLED_PETSC = 1\n')
if bamg.datadir:
  bamgvars.write('DATAFILESPATH = '+bamg.datadir+'\n')

# Write initial part of file bamgconf.h
bamgconf.write('#if !defined(BAMGCONF_H)\n')
bamgconf.write('#define BAMGCONF_H\n\n')
AddDefine(bamgconf,'PETSC_DIR',petsc.dir)
AddDefine(bamgconf,'PETSC_ARCH',petsc.arch)
AddDefine(bamgconf,'DIR',bamg.dir)
AddDefine(bamgconf,'LIB_DIR',os.path.join(bamg.prefixdir,'lib'))
if bamg.isrepo:
  AddDefine(bamgconf,'VERSION_GIT',bamg.gitrev)
  AddDefine(bamgconf,'VERSION_DATE_GIT',bamg.gitdate)
  AddDefine(bamgconf,'VERSION_BRANCH_GIT',bamg.branch)

# Create global configuration file for the case of empty PETSC_ARCH
if emptyarch:
  globconf = bamg.CreateFile(os.path.join(bamg.dir,'lib','bamg','conf'),'bamgvariables')
  globconf.write('BAMG_DIR = '+bamg.dir+'\n')
  globconf.write('PETSC_ARCH = '+archname+'\n')
  globconf.close()

# Check if PETSc is working
log.NewSection('Checking PETSc installation...')
if petsc.nversion > bamg.nversion:
  log.Warn('PETSc version '+petsc.version+' is newer than BAMG version '+bamg.version)
if petsc.isinstall:
  if os.path.realpath(petsc.prefixdir) != os.path.realpath(petsc.dir):
    log.Warn('PETSC_DIR does not point to PETSc installation path')
petsc.Check()
if not petsc.havepackage:
  log.Exit('Unable to link with PETSc')

# Single library installation
if petsc.singlelib:
  bamgvars.write('SHLIBS = libpetscbamg\n')
  bamgvars.write('LIBNAME = '+os.path.join('${INSTALL_LIB_DIR}','libbamg.${AR_LIB_SUFFIX}')+'\n')
  for module in ['SYS','EPS','SVD','PEP','NEP','MFN','LME']:
    bamgvars.write('BAMG_'+module+'_LIB = ${CC_LINKER_SLFLAG}${BAMG_LIB_DIR} -L${BAMG_LIB_DIR} -lpetscbamg ${BAMG_EXTERNAL_LIB} ${PETSC_SNES_LIB}\n')
  bamgvars.write('BAMG_LIB = ${CC_LINKER_SLFLAG}${BAMG_LIB_DIR} -L${BAMG_LIB_DIR} -lpetscbamg ${BAMG_EXTERNAL_LIB} ${PETSC_SNES_LIB}\n')

# Check for external packages and for missing LAPACK functions
for pkg in checkpackages:
  pkg.Process(bamgconf,bamgvars,bamg,petsc,archdir)

# Write Modules and pkg-config configuration files
log.NewSection('Writing various configuration files...')
log.write('Modules file in '+modulesdir)
if bamg.isinstall:
  WriteModulesFile(modules,bamg.lversion,bamg.prefixdir)
else:
  WriteModulesFile(modules,bamg.lversion,bamg.dir)
log.write('pkg-config file in '+pkgconfdir)
WritePkgconfigFile(pkgconfig,bamg.lversion,petsc.version,bamg.dir,bamg.isinstall,bamg.prefixdir)

# Write reconfigure file
if not bamg.isinstall:
  WriteReconfigScript(reconfig,bamg.dir,argdb.UsedArgs())
  try:
    os.chmod(reconfigpath,0o775)
  except OSError as e:
    log.Exit('Unable to make reconfigure script executable:\n'+str(e))

# Finish with configuration files (except bamgvars)
bamgconf.write('\n#endif\n')
bamgconf.close()
pkgconfig.close()
modules.close()
if not bamg.isinstall: reconfig.close()

# Download sowing if requested and make Fortran stubs if necessary
bfort = petsc.bfort
if sowing.downloadpackage:
  bfort = sowing.DownloadAndInstall(bamg,petsc,archdir)

if bamg.isrepo and petsc.fortran:
  try:
    if not os.path.exists(bfort):
      bfort = os.path.join(archdir,'bin','bfort')
    if not os.path.exists(bfort):
      bfort = sowing.DownloadAndInstall(bamg,petsc,archdir)
    log.NewSection('Generating Fortran stubs...')
    log.write('Using BFORT='+bfort)
    sys.path.insert(0, os.path.abspath(os.path.join('lib','bamg','bin','maint')))
    import generatefortranstubs
    generatefortranstubs.main(bamg.dir,bfort,os.getcwd(),0)
    generatefortranstubs.processf90interfaces(bamg.dir,0)
  except:
    log.Exit('Try configuring with --download-sowing or use a git version of PETSc')

if bfort != petsc.bfort:
  bamgvars.write('BFORT = '+bfort+'\n')

# Finally we can close the bamgvariables file
bamgvars.close()

# Print summary
log.NewSection('')
log.Println('')
log.Println('='*80)
log.Println('BAMG Configuration')
log.Println('='*80)
log.Println('\nBAMG directory:\n '+bamg.dir)
if bamg.isrepo:
  log.Println('  It is a git repository on branch: '+bamg.branch)
if bamg.isinstall:
  log.Println('BAMG prefix directory:\n '+bamg.prefixdir)
log.Println('PETSc directory:\n '+petsc.dir)
if petsc.isrepo:
  log.Println('  It is a git repository on branch: '+petsc.branch)
  if bamg.isrepo and petsc.branch!='maint' and bamg.branch!='maint':
    try:
      import dateutil.parser, datetime
      petscdate = dateutil.parser.parse(petsc.gitdate)
      bamgdate = dateutil.parser.parse(bamg.gitdate)
      if abs(petscdate-bamgdate)>datetime.timedelta(days=30):
        log.Warn('Your PETSc and BAMG repos may not be in sync (more than 30 days apart)')
    except ImportError: pass
if emptyarch and bamg.isinstall:
  log.Println('Prefix install with '+petsc.precision+' precision '+petsc.scalar+' numbers')
else:
  log.Println('Architecture "'+archname+'" with '+petsc.precision+' precision '+petsc.scalar+' numbers')
for pkg in checkpackages:
  pkg.ShowInfo()
log.write('\nFinishing Configure Run at '+time.ctime(time.time()))
log.write('='*80)
print()
print('xxx'+'='*74+'xxx')
print(' Configure stage complete. Now build the BAMG library with:')
if emptyarch:
  print('   make BAMG_DIR='+bamg.dir+' PETSC_DIR='+petsc.dir)
else:
  print('   make BAMG_DIR='+bamg.dir+' PETSC_DIR='+petsc.dir+' PETSC_ARCH='+archname)
print('xxx'+'='*74+'xxx')
print()
