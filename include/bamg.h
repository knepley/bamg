#if !defined(BAMGSYS_H)
#define BAMGSYS_H

#include <petscsys.h>

/* ========================================================================== */
/*
   slepcconf.h is created by the configure script and placed in ${PETSC_ARCH}/include.
   It contains macro definitions set at configure time.
*/
#include <bamgconf.h>
/*
    slepcversion.h contains version info
*/
#include <bamgversion.h>
#define BAMG_AUTHOR_INFO "       The BAMG Team\n    petsc-maint@mcs.anl.gov\n https://mcs.anl.gov/petsc\n"

#include <slepceps.h>

PETSC_EXTERN PetscErrorCode BAMGCreateCoarseSpaceEV(PC, PetscInt, DM, KSP, PetscInt, Mat, Mat *);
PETSC_EXTERN PetscErrorCode BAMGCreateCoarseSpaceMEV(PC, PetscInt, DM, KSP, PetscInt, Mat, Mat *);
PETSC_EXTERN PetscErrorCode BAMGCreateCoarseSpaceGEV(PC, PetscInt, DM, KSP, PetscInt, Mat, Mat *);
PETSC_EXTERN PetscErrorCode BAMGCreateCoarseSpaceMGEV(PC, PetscInt, DM, KSP, PetscInt, Mat, Mat *);

#endif
